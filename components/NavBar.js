import { useContext } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'

import UserContext from '../UserContext'

export default function NavBar() {
    //consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext)

    return (
        <Navbar bg="secondary" expand="lg">
            <Link href="/">
                <a className="navbar-brand">Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">

                {(user !== null)   
                ? <React.Fragment>        
                        <Link href="/showCategories">
                            <a className="nav-link" role="button">Categories</a>
                        </Link>
                        <Link href="/records">
                            <a className="nav-link" role="button">Records</a>
                        </Link>
                        <Link href="/breakdown">
                            <a className="nav-link" role="button">Breakdown</a>
                        </Link>
                        <Link href="/logout">
                            <a className="nav-link" role="button">Logout</a>
                        </Link>
                    </React.Fragment>
                    : 
                    <React.Fragment>  
                    <Link href="/login">
                        <a className="nav-link" role="button">Login</a>
                    </Link>
                    <Link href="/register">
                            <a className="nav-link" role="button">Register</a>
                    </Link>
                 </React.Fragment>   
                }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
