import { Row, Col, Jumbotron, Card, Image } from 'react-bootstrap'

export default function Banner(){
	return(
        <React.Fragment>
		<Row>
            <Col>
                <Jumbotron>
                    <h1>Budget Tracker</h1>
                    <p>Spend wise, Invest and save your money to grow!</p>
                    <Row>
                        <Col>
                            <Card className="bg-secondary mb-3">
                                <Card.Body>
                                    <Card.Title>Budget Quotes</Card.Title>
                                    <Card.Text>
                                        <Image src="https://i.pinimg.com/originals/7a/b3/49/7ab349f966cb04b20e0efac4dcbe0a2b.jpg" fluid/>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                        <Card className="bg-secondary mb-3">
                                <Card.Body>
                                    <Card.Title>Budget Quotes</Card.Title>
                                    <Card.Text>
                                        <Image src="https://i.pinimg.com/originals/dd/88/68/dd886860e1df33017f63b1221ab47b96.png" fluid/>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card className="bg-secondary mb-3">
                                <Card.Body>
                                    <Card.Title>Budget Quotes</Card.Title>
                                    <Card.Text>
                                        <Image src="https://i.pinimg.com/564x/53/a0/e0/53a0e0c9b24678d5bb3115aadb06f169.jpg" fluid/>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>

                </Jumbotron>
            </Col>
        </Row>
        </React.Fragment>
	)
}