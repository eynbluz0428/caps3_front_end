module.exports = {
    getAccessToken: () => localStorage.getItem('token'),
    toJSON: (response) => response.json()
}