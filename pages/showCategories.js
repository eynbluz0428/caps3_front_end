import { useState, useEffect } from 'react';
import { Form, Button, Container, Table, Row, Col, Image  } from 'react-bootstrap';
import Router from 'next/router'
import Link from 'next/link'
import Head from 'next/head';

export default function showCategories() {

    const [categories, setCategories] = useState([])

	useEffect(() => {

			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/get-categories`, {
					method: 'POST',
					headers: {
						'Authorization': `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						typeName: undefined
					})	
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					setCategories(data)
				})
			}, [])

		const catergoryRows = categories.map(category => {
			return(
				<tr key={category._id}>
					<td >{category.name}</td>
					<td>{category.type}</td>
				</tr>			
			)
		})

    return (
    	<React.Fragment>
		  <Head>
            <title>Categories</title>
          </Head>
		        <Container>
		            <h3>My Categories</h3>
		            <Table striped bordered hover>
		            		<thead>
		                        <tr>
		                           <th>Category</th>
		                            <th>Type</th>
		                        </tr>
		            		</thead>
		            		<tbody>
		            			{catergoryRows}
		            		</tbody>       	
		            </Table>
		            <Link href="/categories">
		            	<Button variant="success" type="submit">Add Categories</Button>
		            </Link>
		        </Container>
        </React.Fragment>
    )
}
