import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Banner from '../components/Banner';
import {Container} from 'react-bootstrap';

export default function Home() {
  return (
     <React.Fragment> 
          <Head>
            <title>Budget Tracker</title>
          </Head>
          <Banner />    
      </React.Fragment>  
  )
}
