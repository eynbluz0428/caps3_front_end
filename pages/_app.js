import 'bootstrap/dist/css/bootstrap.min.css';
import "bootswatch/dist/minty/bootstrap.min.css"
import '../styles/globals.css'
import NavBar from '../components/NavBar';
import {Container} from 'react-bootstrap';
import { UserProvider } from '../UserContext';
import { useState, useEffect } from 'react';

export default function App({ Component, pageProps }) {
   
    const [user, setUser] = useState(null)

    //effect hook to set global user state when changes to the id property of user state is detected
    useEffect(() => { 		
    	setUser(localStorage.getItem('token')); 	
    }, [])


    const unsetUser = () => {
        localStorage.clear()
        setUser(null)
    }

    return (
        <React.Fragment>
            <UserProvider value={{user, setUser, unsetUser}}>
                <NavBar />
                <Container>
                    <Component {...pageProps} />
                </Container>
            </UserProvider>
        </React.Fragment>
    )
}