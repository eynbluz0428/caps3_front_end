import { useState,useEffect } from 'react';
import { Form, Button, Container, Dropdown, Col, Row, Card } from 'react-bootstrap';
import Router from 'next/router';
import Swal from 'sweetalert2';
import Head from 'next/head';

export default function categories() {

    const [name, setName] = useState('');
    const [type, setType] = useState('');

    function createCategory(e) {
        e.preventDefault();

        console.log(`${name}, ${type}`);
    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/add-category`,{
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: name, 
            typeName: type
        })
    })
        .then(res => res.json())
        .then(data => {
            Swal.fire('You have successfully added a new category')
            Router.push('/records')
        })
        setName('');
        setType('');
    }

    return (
        <React.Fragment>
        <Head>
            <title>Add Categories</title>
        </Head>
        <Container>
            <Form className="mt-5" onSubmit={(e) => createCategory(e)}> 
                <Form.Group controlId="name">
                    <Form.Label>Category Name: </Form.Label>
                    <Form.Control type="text" placeholder="Enter category" value={name} onChange={e => setName(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="type">
                    <Form.Label>Category Type:</Form.Label>
                    <Form.Control as="select" value={type} onChange={e => setType(e.target.value)} required>
                    <option>Select</option>
                    <option value='Income'>Income</option>
                    <option value='Expense'>Expense</option>
                    </Form.Control>
                    <Form.Text className="text-muted">
                    Please select Income or Expense     
                    </Form.Text>
                </Form.Group>
                <Button variant="primary" type="submit">Add</Button>
            </Form>
        </Container>
        </React.Fragment>
    )
}
