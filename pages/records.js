import { useState, useEffect } from 'react';
import { Form, Button, Container, Table, ListGroup, InputGroup, Dropdown} from 'react-bootstrap';
import Router from 'next/router';
import Link from 'next/link';
import Head from 'next/head';
import moment from 'moment';

export default function showTransactions() {

    const [transactions, setTransactions] = useState([])
    const [search, setSearch] = useState('')
    const [searchType, setSearchType] = useState('All')
    const [records, setRecords] = useState('')
    

	useEffect(() => {

			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/get-most-recent-records`, {
					method: 'POST',
					headers: {
						'Authorization': `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						typeName: undefined
					})	
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					setTransactions(data)
				})
			}, [])

	const transactionRecord = transactions.map(transaction => {
			const sign = transaction.type === 'Expense' ? '-' : '+';
			return(

				<ListGroup.Item className="list" key={transaction._id}>	
				<li>{moment(transaction.dateAdded).format('MMM. DD, YYYY')}</li>				
					<li><strong>Description: </strong>{transaction.description}</li>
					<li className={transaction.type === 'Income' ? 'plus' : 'minus'}>
     				<strong>Amount: </strong>{sign}&#8369;{Math.round(transaction.amount)}
    				</li>
					<li className={transaction.type === 'Income' ? 'plus' : 'minus'}>{transaction.type}</li>
					<li>
     				<strong>Available Funds: </strong>&#8369;{Math.abs(transaction.balanceAfterTransaction)}
    				</li>
				</ListGroup.Item>	
			)
		})

	// const availBal = transactions.map(transaction => {
	// 	if(transaction.type === 'Income'){
	// 		  incomeTotal(+= transaction.amount) 
	// 	}else{
	// 		expenseTotal(-=transaction.amount)
	// 	}
	// }


			useEffect(() => {
console.log(search)
console.log(searchType)
			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/search-record`, {

				method: 'POST',
					headers: {
						'Authorization': `Bearer ${localStorage.getItem('token')}`,
						'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							searchType: searchType,
		                   	searchKeyword: search
						})	
					})
					.then(res => res.json())
					.then(data => {
						// console.log(data)
						setTransactions(data)
					})	
			},[search, searchType])



    return (
    	<React.Fragment>
    	<Head>
    		<title>Transaction Record</title>
    	</Head>
    	  <InputGroup size="lg" >
		    <InputGroup.Prepend >
		    <Link href='/addRecords'>
		      <Button variant="success">Add</Button>
		    </Link>    
		    </InputGroup.Prepend>
		    <Form.Control type="text" placeholder="Search for description
		    " value={search} onChange={(e) => setSearch(e.target.value)} ></Form.Control>
		    <Form.Control as="select" value={searchType} onChange={(e) => setSearchType(e.target.value)} placeholder="Search for a Record">
		            <option value='All'>All</option>
                    <option value='Income'>Income</option>
                    <option value='Expense'>Expense</option>
            </Form.Control>
		  </InputGroup>
	    	<ListGroup>
	    		{transactionRecord}
	    	</ListGroup>
    	</React.Fragment>
    )
}
