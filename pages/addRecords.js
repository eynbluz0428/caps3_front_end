import { useState, useEffect } from 'react';
import { Form, Button, Container, Dropdown, Card, Col, Row  } from 'react-bootstrap';
import Router from 'next/router'
import Swal from 'sweetalert2';
import toNum from '../toNum'
import Head from 'next/head';
import Link from 'next/link'

export default function records() {

    const [categoryName, setCategoryName] = useState('');
    const [type, setType] = useState('');
    const [amount, setAmount] = useState ('')
    const [description, setDescription] = useState('')

    const [categoryData, setCategoryData] = useState([])

    useEffect(() => {

            fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/get-categories`, {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        typeName: type
                    })  
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    setCategoryData(data)
                })
            }, [type])


//Add Record
    function addRecord(e) {
        e.preventDefault();

        console.log(`${categoryName}, ${type}, ${amount}, ${description}`);


    fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/add-record`,{
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            categoryName: categoryName, 
            typeName: type,
            amount: toNum(amount),
            description: description
        })
    })
        .then(res => res.json())
        .then(data => {
            Swal.fire('You have added a new record')
            Router.push('/records')
        })
        setCategoryName('')
        setType('')
        setAmount('')
        setDescription('')
    }

    return (
        <Container>
        <Head>
            <title>Add Records</title>
        </Head>

        <Card>
            <Card.Header>Add your records</Card.Header>
            <Card.Body>
            <Form className="mt-5" onSubmit={(e) => addRecord(e)}>
                <Form.Group controlId="type">
                    <Form.Label>Category Type:</Form.Label>
                    <Form.Control as="select" value={type} onChange={e => setType(e.target.value)} required>
                    <option>Select</option>
                    <option value='Income'>Income</option>
                    <option value='Expense'>Expense</option>
                    </Form.Control>
                    <Form.Text className="text-muted">
                    Please select Income or Expense     
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="categoryName">
                    <Form.Label>Category Name: </Form.Label>
                    <Form.Control as="select" placeholder="Enter category" onChange={e => setCategoryName(e.target.value)} required>
                        <option>Select</option>
                            {categoryData.map(categories => {
                                return(
                                    <option key={categories._id} value={categories.name}>{categories.name}</option>
                                )
                            })}   
                    </Form.Control>    
                </Form.Group>
                <Form.Group controlId="amount">
                    <Form.Label>Amount: </Form.Label>
                    <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="description">
                    <Form.Label>Description: </Form.Label>
                    <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
                </Form.Group>
                 <Button variant="primary" type="submit">Add Records</Button>                
            </Form>
            </Card.Body>
        </Card>
        </Container>
    )
}
