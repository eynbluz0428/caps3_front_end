import { useEffect, useState } from 'react';
import moment from 'moment';
import { Tabs, Tab, } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2'
import Head from 'next/head';

export default function breakdown(){
    const [income, setIncome] = useState([])
    const [expense, setExpense] = useState([])
    const [totalIncome, setTotalIncome] = useState([])
   	const [totalExpense, setTotalExpense] = useState([])

	useEffect(() => {

			fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/users/get-records`, {
				method: 'POST',
					headers: {
						'Authorization': `Bearer ${localStorage.getItem('token')}`
						}	
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data)
					if(data.length > 0){
						setIncome(data.filter(record => {
						return record.type === 'Income';
						}))
						setExpense(data.filter(record => {
						return record.type === 'Expense';
					}))
					}	
				})	

			},[])

	useEffect(() =>{
		console.log(expense)
		console.log(income)	
		let incomeArr = [0,0,0,0,0,0,0,0,0,0,0,0]
			income.forEach(record => {
			incomeArr[new Date(record.dateAdded).getMonth()] += record.amount;
		})
			setTotalIncome(incomeArr);

		let expenseArr = [0,0,0,0,0,0,0,0,0,0,0,0]
			expense.forEach(record => {
			expenseArr[new Date(record.dateAdded).getMonth()] += record.amount;
		})
			setTotalExpense(expenseArr);

	},[income,expense])

	 const incomeData = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [
            {
                label: 'Monthly Income in Php',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: totalIncome
            }
        ]
    }

    const expenseData = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [
            {
                label: 'Monthly Expense in Php',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: totalExpense
            }
        ]
    }

	return(
		<React.Fragment>
		 <Head>
            <title>Monthly Summary</title>
        </Head>
		<h1 className='text-center'>Hello to your Monthly Budget Summary</h1>
			<Tabs defaultActiveKey="distances" id="monthlyFigures">
				<Tab eventKey="income" title="Monthly Income Summary">
					<Bar data={incomeData}/>
				</Tab>
				<Tab eventKey="expense" title="Monthly Expense Summary">
					<Bar data={expenseData}/>
				</Tab>
			</Tabs>
		</React.Fragment>
	)
}